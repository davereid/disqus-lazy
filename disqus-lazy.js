(function (window) {

    "use strict";

    var disqusThread = document.getElementById('disqus_thread'),
        disqusLoaded = false,
        disqusLazyLoadOffset = disqusLazyLoadOffset || 100;

    function loadDisqus() {
        var dsq = document.createElement('script');
        dsq.type = 'text/javascript';
        dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        disqusLoaded = true;
    }

    // Get the offset of an object
    function findTop(obj) {
        var curtop = 0;
        if (obj.offsetParent) {
            do {
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
            return curtop;
        }
    }

    if (window.location.hash === '#disqus_thread') {
        loadDisqus();
    }
    else {
        var disqusThreadOffset = findTop(disqusThread);
        window.onscroll = function() {
            if (!disqusLoaded && window.pageYOffset > disqusThreadOffset - disqusLazyLoadOffset)
                loadDisqus();
        }
    }

})(this);
