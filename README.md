# Disqus Lazy Load

Allow Disqus threads to be lazy loaded when scrolled into view.

## Usage

If using jQuery, use disqus-lazy.jquery.js. Otherwise use disqus-lazy.js.

Simply add the Disqus thread HTML:

~~~html
<div id="disqus_thread"></div>
<script type="text/javascript">
    var disqus_shortname = 'mysite';
</script>
<noscript>Please enable JavaScript to view the
    <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered byDisqus.</a>
</noscript>
<script src="disqus-lazy.js" defer></script>
~~~

## Original sources

* http://christian.fei.ninja/how-to-lazy-load-disqus-comments/
* https://gist.github.com/CrocoDillon/5727950
